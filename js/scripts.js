// init time stamps
(function() {
  const timeRangeHtml = [];
  const timeBlocks = [];
  let hour = 9;
  let minutes = '00';
  let meridiem = 'AM';

  for (let index = 0; index < 23; index++) {
    timeBlocks.push('<div class="time-block"></div>');
    timeRangeHtml.push(
      `<div class="hour${index % 2 ? ' text-muted' : ''}">${hour}:${minutes}${index % 2 ? '' : `<span class="text-muted"> ${meridiem}</span>`}</div>`
    );
    if (index % 2) {
      ++hour;
      minutes = '00';
    } else {
      minutes = '30';
    }
    if (hour === 12) {
      hour = 1;
      meridiem = 'PM';
    }
  }

  const timeSection = document.querySelector('.time');
  timeSection.innerHTML = timeRangeHtml.join('');

  document.querySelector('.time-blocks').innerHTML = timeBlocks.join('');
})();

const events = [
  { start: 30, end: 150 },
  { start: 540, end: 600 },
  { start: 560, end: 620 },
  { start: 610, end: 670 }
];

/**
 *
 * @param {{start:number, end: number}[]} input
 */
function layOutDay(input) {
  const events = getCollisons(input);
  const timeBlocks = document.querySelectorAll('.time-block');
  events.sort((a, b) => a.start - b.start);
  const eventsHtml = events
    .map(event => {
      const { top, height } = getLocation(event, timeBlocks);
      const cCount = event.collisions.reduce((acc, curr) => {
        acc = event.collisions.length + 1;
        const foundEvent = events.find(e => e.start === curr.start && e.end === curr.end);

        if (foundEvent) {
          if (event.start <= foundEvent.start) {
            event.collisions.length > 1 ? (foundEvent.order = foundEvent.order + event.collisions.length) : foundEvent.order++;
          }
          if (foundEvent.collisions.length > event.collisions.length) acc = foundEvent.collisions.length + 1;
        }
        return acc;
      }, 1);

      return `<div class="event" style="top: ${top}px; height: ${height}px; width: ${100 / cCount}%; left: ${
        event.order ? (100 / cCount) * event.order : 0
      }%"><div class="title">Sample Item</div><div class="description text-muted">Sample Location</div></div>`;
    })
    .join('');
  document.querySelector('.events').innerHTML = eventsHtml;
}

/**
 * gets the starting top position and the height of the box
 * @param {{start:number, end: number}} event
 * @param {NodeListOf<HTMLElement>} sections
 * @returns {{top: number, height: number}}
 */
function getLocation({ start, end }, sections) {
  const topPosition = start / 30;
  const topOffset = (topPosition % 1) * 30;
  const closestTopElOffset = sections[Math.floor(topPosition)].offsetTop;
  const top = closestTopElOffset + topOffset;

  const baseHeight = (end - start) / 30;
  const heightOffset = (baseHeight % 1) * 30;
  const height = Math.floor(baseHeight) * 32.7 + heightOffset;
  return { top, height };
}

// finds all colliding events per given event
function checkCollison(event, events) {
  return events.reduce((acc, curr) => {
    if (event.start === curr.start && curr.end === event.end) return acc;
    if (event.start < curr.end && event.end > curr.start) acc.push(curr);
    return acc;
  }, []);
}

/**
 * finds all colliding events per event and addes them to an array
 * @return {{start:number, end: number, collisions: {start:number, end: number}[]}[]}
 */
function getCollisons(events) {
  return events.map(event => ({
    ...event,
    order: 0,
    collisions: checkCollison(event, events)
  }));
}

layOutDay(events);
